package dev.mattkds.vettoolsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VetToolsApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(VetToolsApiApplication.class, args);
    }

}
