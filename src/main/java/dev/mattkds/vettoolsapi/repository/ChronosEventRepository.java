package dev.mattkds.vettoolsapi.repository;

import dev.mattkds.vettoolsapi.controllers.dto.ChronosEventDto;
import dev.mattkds.vettoolsapi.models.ChronosEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface ChronosEventRepository extends JpaRepository<ChronosEvent, Long>, JpaSpecificationExecutor<ChronosEvent> {

    @Query("SELECT c FROM ChronosEvent c WHERE c.isActive = true AND c.memberId = :id AND c.startTime < :startTime AND c.startTime > :startTimeAfter")
    public Optional<ChronosEvent> findActiveChronosEventFromUserIdentifierFromDateAndIsActive(long id, LocalDateTime startTime, LocalDateTime startTimeAfter);

    @Query("SELECT c FROM ChronosEvent c WHERE c.memberId = :id AND c.startTime < :startTime AND c.startTime > :startTimeAfter")
    List<ChronosEvent> findActiveChronosEventFromUserIdentifierFromDateCustom(Long id, LocalDateTime startTime, LocalDateTime startTimeAfter);
}
