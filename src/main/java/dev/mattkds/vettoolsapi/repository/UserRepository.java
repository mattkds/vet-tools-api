package dev.mattkds.vettoolsapi.repository;

import dev.mattkds.vettoolsapi.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}

