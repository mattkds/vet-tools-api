package dev.mattkds.vettoolsapi.repository.specifications;

import dev.mattkds.vettoolsapi.models.ChronosEvent;
import org.springframework.data.jpa.domain.Specification;

public class ChronosEventSpecification {

    public ChronosEventSpecification() {}

    public static Specification<ChronosEvent> isActiveSpecification(boolean isActive) {
        return (root, query, builder) -> builder.equal(root.get("isActive"), isActive);
    }

}
