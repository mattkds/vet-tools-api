package dev.mattkds.vettoolsapi.models;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Data
@Table(name = "chronos_event")
public class ChronosEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "identifier")
    private Long identifier;

    @Column(name = "start_time")
    private LocalDateTime startTime;

    @Column(name = "end_time")
    private LocalDateTime endTime;

    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    @Column(name = "updated_date")
    private LocalDateTime updatedDate;

    @Column(name = "member_id")
    private Long memberId;

    @Column(name = "creator_id")
    private Long creatorId;

    @Column(name = "is_active")
    private boolean isActive;

    @Column(name = "note")
    private String note;
}

