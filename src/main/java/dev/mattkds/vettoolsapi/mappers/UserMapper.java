package dev.mattkds.vettoolsapi.mappers;

import dev.mattkds.vettoolsapi.controllers.dto.UserDto;
import dev.mattkds.vettoolsapi.models.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    /**
     * Mapper to transform a user Entity to a userDto
     * @param user the entity
     * @return the userDto
     */
    public UserDto toDto(User user);

    /**
     * Mapper to transform a user Dto to a user entity
     * @param userDto the userDto
     * @return the entity
     */
    public User toEntity(UserDto userDto);
}
