package dev.mattkds.vettoolsapi.mappers;


import dev.mattkds.vettoolsapi.controllers.dto.ChronosEventDto;
import dev.mattkds.vettoolsapi.models.ChronosEvent;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ChronosEventMapper {

    /**
     * Mapper to transform a chronos Event Entity to a chronosEventDto
     * @param chronosEvent the entity
     * @return the userDto
     */
    public ChronosEventDto toDto(ChronosEvent chronosEvent);

    /**
     * Mapper to transform a chronosEvent Dto to a chronos Event entity
     * @param chronosEventDto the chronosEventDto
     * @return the entity
     */
    public ChronosEvent toEntity(ChronosEventDto chronosEventDto);
}
