package dev.mattkds.vettoolsapi.exception;

public class BadRequestException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    public BadRequestException(String customMessage) {
        super(customMessage);
    }
}
