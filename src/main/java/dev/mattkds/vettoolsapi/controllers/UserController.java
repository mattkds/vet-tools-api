package dev.mattkds.vettoolsapi.controllers;

import dev.mattkds.vettoolsapi.controllers.api.UserApi;
import dev.mattkds.vettoolsapi.controllers.dto.UserDto;
import dev.mattkds.vettoolsapi.exception.BadRequestException;
import dev.mattkds.vettoolsapi.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController implements UserApi {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Override
    public ResponseEntity<List<UserDto>> getUsers() {
        try {
            return ResponseEntity.ok(this.userService.getAllUsers());
        } catch (Exception e) {
            throw new BadRequestException("Error while retrieving users");
        }
    }
}
