package dev.mattkds.vettoolsapi.controllers.utils;

public class UserEndpointUtils {
    public static final String USERS = "/users";
    public static final String BY_IDENTIFIER = "/{identifier}";
}
