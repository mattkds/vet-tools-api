package dev.mattkds.vettoolsapi.controllers.utils;

public class ChronosEndpointUtils {
    public static final String CHRONOS = "/chronos";
    public static final String BY_IDENTIFIER = "/{identifier}";
    public static final String STOP = "/stop";
    public static final String UNIQUE_ACTIVE = "/active";
    public static final String LIST = "/list";

}
