package dev.mattkds.vettoolsapi.controllers;


import dev.mattkds.vettoolsapi.exception.BadRequestException;
import dev.mattkds.vettoolsapi.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@ResponseBody
public class ExceptionControllerAdvice {

    /**
     * Handler for the NotFoundException
     * @param ex the exception
     * @return a new ResponseEntity with not found Http Status
     */
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<String> resourceNotFoundException(NotFoundException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    /**
     * Handler for the BadRequestException
     * @param ex the exception
     * @return a new ResponseEntity with not found Http Status
     */
    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<String> resourceBadRequestException(BadRequestException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
