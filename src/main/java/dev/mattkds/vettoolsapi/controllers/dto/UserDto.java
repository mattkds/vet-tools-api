package dev.mattkds.vettoolsapi.controllers.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private Long identifier;
    private String firstname;
    private String lastname;
}
