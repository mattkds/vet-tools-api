package dev.mattkds.vettoolsapi.controllers.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort.Direction;

@Data
@NoArgsConstructor
public class ChronosEventCriteria {
    private Integer pageNumber;
    private Integer pageSize;
    private Direction sortDirection;
    private String sortBy;
    private Boolean isActiveEquals;
}
