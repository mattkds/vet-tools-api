package dev.mattkds.vettoolsapi.controllers.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateChronosDto {
    private Long memberId;
    private Long creatorId;
}
