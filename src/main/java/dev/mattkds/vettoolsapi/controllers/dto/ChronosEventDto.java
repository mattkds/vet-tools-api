package dev.mattkds.vettoolsapi.controllers.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChronosEventDto {
    private Long identifier;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Long memberId;
    private Long creatorId;
    private boolean isActive;
    private String note;
}
