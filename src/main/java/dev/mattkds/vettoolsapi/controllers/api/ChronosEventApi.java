package dev.mattkds.vettoolsapi.controllers.api;

import dev.mattkds.vettoolsapi.controllers.dto.ChronosEventCriteria;
import dev.mattkds.vettoolsapi.controllers.dto.ChronosEventDto;
import dev.mattkds.vettoolsapi.controllers.dto.CreateChronosDto;
import dev.mattkds.vettoolsapi.controllers.utils.ChronosEndpointUtils;
import dev.mattkds.vettoolsapi.controllers.utils.EndpointsUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RequestMapping(EndpointsUtils.BASE + ChronosEndpointUtils.CHRONOS)
public interface ChronosEventApi {

    @Operation(summary = "Récupérer la liste des events", description = "Récupère toutes les events")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Liste des events récupérée avec succès"),
            @ApiResponse(responseCode = "500", description = "Erreur interne du serveur")
    })
    @GetMapping
    ResponseEntity<Page<ChronosEventDto>> getChronosEvents(@ParameterObject ChronosEventCriteria chronosEventCriteria);

    @Operation(summary = "Récupérer un chrono event actif depuis une date et relié à un user", description = "Récupère toutes les events")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reseponse ok"),
            @ApiResponse(responseCode = "500", description = "Erreur interne du serveur")
    })
    @GetMapping(ChronosEndpointUtils.UNIQUE_ACTIVE)
    ResponseEntity<ChronosEventDto> retrieveActiveChronosEventFromUserAndDateIsActive(@RequestParam LocalDate date, @RequestParam Integer userIdentifier);


    @Operation(summary = "Récupérer un chrono event actif depuis une date et relié à un user", description = "Récupère toutes les events")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reseponse ok"),
            @ApiResponse(responseCode = "500", description = "Erreur interne du serveur")
    })
    @GetMapping(ChronosEndpointUtils.LIST)
    ResponseEntity<List<ChronosEventDto>> retrieveActiveChronosEventFromUserAndDate(@RequestParam LocalDate date, @RequestParam Integer userIdentifier);


    @Operation(summary = "Création d'un event", description = "Créer un nouvel event")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "L'event a été créé avec succès"),
            @ApiResponse(responseCode = "500", description = "Erreur interne du serveur")
    })
    @PostMapping()
    ResponseEntity<ChronosEventDto> createNewEvent(@RequestBody CreateChronosDto chronosEventDto);

    @Operation(summary = "Mise à jour d'un event", description = "Mise à jour d'un event")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "L'event a été mis à jour avec succès"),
            @ApiResponse(responseCode = "500", description = "Erreur interne du serveur")
    })
    @PutMapping()
    ResponseEntity<ChronosEventDto> updateEvent(@RequestBody ChronosEventDto chronosEventDto);

    @Operation(summary = "Arret d'un chrono event", description = "Arret d'un chrono event")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "L'event a été mis à jour avec succès"),
            @ApiResponse(responseCode = "500", description = "Erreur interne du serveur")
    })
    @PutMapping(ChronosEndpointUtils.STOP + ChronosEndpointUtils.BY_IDENTIFIER)
    ResponseEntity<ChronosEventDto> stopChronosEvent(@PathVariable Integer identifier);

    @Operation(summary = "Suppression d'un chrono event", description = "Suppression d'un chrono event")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "L'event bien été supprimé"),
            @ApiResponse(responseCode = "500", description = "Erreur interne du serveur")
    })
    @DeleteMapping(ChronosEndpointUtils.BY_IDENTIFIER)
    ResponseEntity<Void> deleteChronosEvent(@PathVariable Integer identifier);

}
