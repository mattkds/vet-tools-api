package dev.mattkds.vettoolsapi.controllers.api;

import dev.mattkds.vettoolsapi.controllers.dto.UserDto;
import dev.mattkds.vettoolsapi.controllers.utils.EndpointsUtils;
import dev.mattkds.vettoolsapi.controllers.utils.UserEndpointUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequestMapping(EndpointsUtils.BASE + UserEndpointUtils.USERS)
public interface UserApi {

    @Operation(summary = "Récupérer la liste des utilisateurs", description = "Récupère toutes les utilisateurs de l'application.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Liste des utilisateurs récupérée avec succès"),
            @ApiResponse(responseCode = "500", description = "Erreur interne du serveur")
    })
    @GetMapping
    ResponseEntity<List<UserDto>> getUsers();
}
