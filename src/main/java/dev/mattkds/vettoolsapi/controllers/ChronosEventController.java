package dev.mattkds.vettoolsapi.controllers;

import dev.mattkds.vettoolsapi.controllers.api.ChronosEventApi;
import dev.mattkds.vettoolsapi.controllers.dto.ChronosEventCriteria;
import dev.mattkds.vettoolsapi.controllers.dto.ChronosEventDto;
import dev.mattkds.vettoolsapi.controllers.dto.CreateChronosDto;
import dev.mattkds.vettoolsapi.exception.BadRequestException;
import dev.mattkds.vettoolsapi.mappers.ChronosEventMapper;
import dev.mattkds.vettoolsapi.models.ChronosEvent;
import dev.mattkds.vettoolsapi.services.ChronosEventService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200", "https://vettools-dev-app.mattkds.dev"})
@RestController
@Slf4j
public class ChronosEventController implements ChronosEventApi {

    private final ChronosEventService chronosEventService;

    public ChronosEventController(ChronosEventService chronosEventService) {
        this.chronosEventService = chronosEventService;
    }

    @Override
    public ResponseEntity<Page<ChronosEventDto>> getChronosEvents(ChronosEventCriteria criteria) {
        try {
            return ResponseEntity.ok(this.chronosEventService.getChronosEvents(criteria));
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestException("Error while retrieving chronos events");
        }
    }

    /**
     *
     * @param date
     * @param userIdentifier
     * @return
     */
    @Override
    public ResponseEntity<ChronosEventDto> retrieveActiveChronosEventFromUserAndDateIsActive(LocalDate date, Integer userIdentifier) {
        try {
            return ResponseEntity.ok(this.chronosEventService.getChronosEventFromUserAndDateIsActive(date, userIdentifier));
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestException("Error while retrieving chronos events");
        }
    }

    /**
     *
     * @param date
     * @param userIdentifier
     * @return
     */
    @Override
    public ResponseEntity<List<ChronosEventDto>> retrieveActiveChronosEventFromUserAndDate(LocalDate date, Integer userIdentifier) {
        try {
            return ResponseEntity.ok(this.chronosEventService.getChronosEventFromUserAndDate(date, userIdentifier));
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestException("Error while retrieving chronos events");
        }
    }

    @Override
    public ResponseEntity<ChronosEventDto> createNewEvent(CreateChronosDto createChronosDto) {
        try {
            return ResponseEntity.ok(this.chronosEventService.createChronosEvent(createChronosDto));
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestException("Error while retrieving chronos events");
        }
    }

    @Override
    public ResponseEntity<ChronosEventDto> updateEvent(ChronosEventDto chronosEventDto) {
        try {
            return ResponseEntity.ok(this.chronosEventService.updateChronosEvent(chronosEventDto));
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestException("Error while retrieving chronos events");
        }
    }

    @Override
    public ResponseEntity<ChronosEventDto> stopChronosEvent(@PathVariable("identifier") Integer chronosIdentifier) {
            return ResponseEntity.ok(this.chronosEventService.stopChronosEvent(chronosIdentifier));
    }

    @Override
    public ResponseEntity<Void> deleteChronosEvent(Integer identifier) {
        try {
            this.chronosEventService.deleteChronosEvent(identifier);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (RuntimeException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
