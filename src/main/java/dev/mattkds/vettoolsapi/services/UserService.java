package dev.mattkds.vettoolsapi.services;

import dev.mattkds.vettoolsapi.controllers.dto.UserDto;
import dev.mattkds.vettoolsapi.mappers.UserMapper;
import dev.mattkds.vettoolsapi.models.User;
import dev.mattkds.vettoolsapi.repository.UserRepository;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserService(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    public List<UserDto> getAllUsers() {
        return userRepository.findAll().stream().map(userMapper::toDto).toList();
    }

    public User createUser(User user) {
        return userRepository.save(user);
    }

    // Ajoutez d'autres méthodes pour la mise à jour, la suppression, etc.
}

