package dev.mattkds.vettoolsapi.services;

import dev.mattkds.vettoolsapi.controllers.dto.ChronosEventCriteria;
import dev.mattkds.vettoolsapi.controllers.dto.ChronosEventDto;
import dev.mattkds.vettoolsapi.controllers.dto.CreateChronosDto;
import dev.mattkds.vettoolsapi.exception.NotFoundException;
import dev.mattkds.vettoolsapi.mappers.ChronosEventMapper;
import dev.mattkds.vettoolsapi.models.ChronosEvent;
import dev.mattkds.vettoolsapi.repository.ChronosEventRepository;
import dev.mattkds.vettoolsapi.repository.specifications.ChronosEventSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Service
public class ChronosEventService {

    private final ChronosEventRepository chronosEventRepository;
    private final ChronosEventMapper chronosEventMapper;

    public ChronosEventService(ChronosEventRepository chronosEventRepository, ChronosEventMapper chronosEventMapper) {
        this.chronosEventRepository = chronosEventRepository;
        this.chronosEventMapper = chronosEventMapper;
    }

    public Page<ChronosEventDto> getChronosEvents(ChronosEventCriteria chronosEventCriteria) {
        Sort.Direction sortDirection = chronosEventCriteria.getSortDirection() != null ? chronosEventCriteria.getSortDirection() : Sort.Direction.ASC;
        String sortBy = chronosEventCriteria.getSortBy() != null ? chronosEventCriteria.getSortBy() : "identifier";
        Pageable page = PageRequest.of(chronosEventCriteria.getPageNumber(), chronosEventCriteria.getPageSize(), sortDirection, sortBy);
        Page<ChronosEvent> pageOfChronosEvent =  this.chronosEventRepository.findAll(createSpecification(chronosEventCriteria), page);
        return pageOfChronosEvent.map(chronosEventMapper::toDto);
    }

    private Specification<ChronosEvent> createSpecification(ChronosEventCriteria chronosEventCriteria) {
        Specification<ChronosEvent> specifications = Specification.where(null);
        if (chronosEventCriteria != null) {
            if (chronosEventCriteria.getIsActiveEquals() != null) {
                specifications = specifications.and(ChronosEventSpecification.isActiveSpecification(chronosEventCriteria.getIsActiveEquals()));
            }
        }
        return specifications;
    }

    /**
     * Create a chronos event
     * @param createChronosDto
     * @return the chronos Event created
     */
    public ChronosEventDto createChronosEvent(CreateChronosDto createChronosDto) {
        ChronosEvent chronosEventToAdd = new ChronosEvent();
        chronosEventToAdd.setStartTime(LocalDateTime.now());
        chronosEventToAdd.setCreationDate(LocalDateTime.now());
        chronosEventToAdd.setUpdatedDate(LocalDateTime.now());
        chronosEventToAdd.setActive(true);
        chronosEventToAdd.setCreatorId(createChronosDto.getCreatorId());
        chronosEventToAdd.setMemberId(createChronosDto.getMemberId());
        return this.chronosEventMapper.toDto(this.chronosEventRepository.save(chronosEventToAdd));
    }

    public ChronosEventDto updateChronosEvent(ChronosEventDto chronosEventDto) {
        Optional<ChronosEvent> chronosEventToUpdate = this.chronosEventRepository.findById(chronosEventDto.getIdentifier());
        if (chronosEventToUpdate.isPresent()) {
            ChronosEvent chronosEventToUpdateEntity = chronosEventMapper.toEntity(chronosEventDto);
            this.chronosEventRepository.save(chronosEventToUpdateEntity);
        }
        return chronosEventDto;
    }

    /**
     * Stop a chronos Event
     * @param chronosIdentifier
     * @return the chronos event updated
     */
    public ChronosEventDto stopChronosEvent(Integer chronosIdentifier) {
        Optional<ChronosEvent> chronosEventToUpdate = this.chronosEventRepository.findById(Long.valueOf(chronosIdentifier));
        if (chronosEventToUpdate.isPresent()) {
            ChronosEvent chronosEventToUpdateEntity = chronosEventToUpdate.get();
            chronosEventToUpdateEntity.setEndTime(LocalDateTime.now());
            chronosEventToUpdateEntity.setActive(false);
            this.chronosEventRepository.save(chronosEventToUpdateEntity);
            return this.chronosEventMapper.toDto(chronosEventToUpdateEntity);
        } else {
            throw new NotFoundException("The chronos event to stop has not been founded");
        }
    }

    /**
     *
     * @param date
     * @param userIdentifier
     * @return
     */
    public ChronosEventDto getChronosEventFromUserAndDateIsActive(LocalDate date, Integer userIdentifier) {
        Optional<ChronosEvent> chronosEvent = this.chronosEventRepository.findActiveChronosEventFromUserIdentifierFromDateAndIsActive(Long.valueOf(userIdentifier), LocalDateTime.of(date, LocalTime.MAX),LocalDateTime.of(date, LocalTime.MIN));
        return chronosEvent.map(chronosEventMapper::toDto).orElse(null);
    }

    /**
     *
     * @param date
     * @param userIdentifier
     * @return
     */
    public List<ChronosEventDto> getChronosEventFromUserAndDate(LocalDate date, Integer userIdentifier) {
        List<ChronosEvent> chronosEvents = this.chronosEventRepository.findActiveChronosEventFromUserIdentifierFromDateCustom(Long.valueOf(userIdentifier), LocalDateTime.of(date, LocalTime.MAX),LocalDateTime.of(date, LocalTime.MIN));
        return chronosEvents.stream().map(chronosEventMapper::toDto).toList();
    }

    /**
     * Delete a chronos Event
     * @param identifier
     * @return
     */
    public void deleteChronosEvent(Integer identifier) {
        this.chronosEventRepository.deleteById(Long.valueOf(identifier));
    }
}
