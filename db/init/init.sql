CREATE TABLE users (
  identifier SERIAL PRIMARY KEY,
  firstName VARCHAR(255) NOT NULL,
  lastName VARCHAR(255) NOT NULL
);

CREATE TABLE chronos_event (
   identifier SERIAL PRIMARY KEY,
   start_time TIMESTAMP NOT NULL,
   end_time TIMESTAMP,
   creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   updated_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   member_id INTEGER NOT NULL,
   creator_id INTEGER NOT NULL,
   is_active BOOLEAN DEFAULT TRUE,
   note TEXT,
   CONSTRAINT fk_member FOREIGN KEY (member_id) REFERENCES users (identifier),
   CONSTRAINT fk_creator FOREIGN KEY (creator_id) REFERENCES users (identifier)
);


