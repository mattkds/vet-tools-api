FROM openjdk:21-slim

ENV JAVA_OPTS=""

COPY ./target/*.jar /app.jar

EXPOSE 8080

ENTRYPOINT ["sh", "-c", "exec java $JAVA_OPTS -jar /app.jar"]